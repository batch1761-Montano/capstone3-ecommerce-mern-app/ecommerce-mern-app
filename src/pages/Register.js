import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';

export default function Register () {

    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    //state hooks to store the values of the input fields.
    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ verifyPassword, setVerifyPassword ] = useState('');

    //state for the enable/disable button
    const [ isActive, setIsActive ] = useState(true); 

    useEffect(() => {
        //Validation to enable submit button
        if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
            setIsActive(true);
        }else {
            setIsActive(false);   
        }
    }, [email, password, verifyPassword])


    function registerUser(e) {
        e.preventDefault();

        //Clear input fields
        fetch('https://bread-n-butter-ecommerce.herokuapp.com/users/register', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data){

                if(data.message === `Email already existed. Do you wish to login?`) {
                    Swal.fire({
                    title: 'error',
                    icon: 'error',
                    text: 'Email already existed. Do you wish to login?'
                    })

                } else {
                    Swal.fire({
                      position: 'top-end',
                      icon: 'success',
                      title: `Registration Successful!`,
                      showConfirmButton: false,
                      timer: 2000
                    })
                    navigate('/login')
                }

            } 
            else {
                Swal.fire({
                    title: 'error',
                    icon: 'error',
                    text: 'Please try again'
                })
            }

        })

}

    return(
            
            (user.accessToken !== null) ?
            <Navigate to="/products" />

            :

    <Row className="mt-3 mx-3">
        <Col md={{ span:10, offset: 1 }} lg ={{span:8, offset: 2 }} >{``}
            <Container fluid ="lg" className="border border-danger rounded p-3 ">
            <Form onSubmit={e => registerUser(e)}>
                <h1 className='mt-4'>Register</h1>
                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control 
                            type="email"
                            placeholder="Please enter your email" 
                            required
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.</Form.Text>        
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                    type="password"
                    placeholder="Please enter your password" 
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                    type="password"
                    placeholder="Verify password" 
                    required
                    value={verifyPassword}
                    onChange={e => setVerifyPassword(e.target.value)}
                    />
                </Form.Group>
                { isActive ?
                <Button className="mt-4" variant="primary" type="submit">Submit</Button>
                :
                <Button className="mt-4" variant="primary" type="submit" disabled>Submit</Button>

                }
            </Form>
            </Container>
        </Col>
    </Row>
    )
}