import { useState, useContext, useEffect } from 'react';
import { Container, Row, Col, Card, Button,} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useParams, Link} from 'react-router-dom';
import { GrFormPreviousLink } from "react-icons/gr";
import { AiOutlineShoppingCart } from 'react-icons/ai';


export default function SpecificProduct() {

	const { productId } = useParams();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [subTotal, setSubTotal] = useState(0);
	const [cart, setCart] = useState([]);
	const [image, setImage] = useState('');


	useEffect(() => {

		fetch(`https://bread-n-butter-ecommerce.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImage(data.image)
		})

		setSubTotal(quantity * price)

	}, [subTotal, quantity, cart, price, productId])


	const { user } = useContext(UserContext);


	//order function
	function decrementQuantity () {
		setQuantity(prevQuantity => prevQuantity - 1)
		if(quantity === 0) {
			setQuantity(0)
		}
	}

	function incrementQuantity () {
		setQuantity(prevQuantity => prevQuantity + 1)
	}

	const addToCart =  (itemId, quant, sub) => {
		if( quant === 0) {
			Swal.fire({
				title: 'Quantity cannot be zero',
				icon: 'error'
			})
		} else {
			//new data every time addToCart is invoked
			let newCartItem = {
				productId: itemId,
				name: name,
				price: price,
				quantity: quant,
				subTotal: sub
			}

			//save an empty array if nothing is stored yet
			if(localStorage.getItem('cartitems') == null){
				localStorage.setItem('cartitems', '[]')
			}

			//gets previously stored data if there's any
			let storedData = JSON.parse(localStorage.getItem('cartitems'));

			let newCartArray = [];
			let isExisting = false;

			for (let i = 0; i < storedData.length ; i++) {
				if (storedData[i].productId === itemId){
					storedData[i].quantity += quant;
					storedData[i].subTotal += subTotal;
					isExisting = true;
				}
				newCartArray.push(storedData[i])
			}

			if (isExisting === false) {
				newCartArray.push(newCartItem)
			}

			localStorage.setItem('cartitems', JSON.stringify(newCartArray));
			
			Swal.fire({
			  position: 'center',
			  icon: 'success',
			  title: `Successfully added ${name} to your cart!`,
			  showConfirmButton: false,
			  timer: 1500
			})
		}
		
	}



	return(
		<Container>
			<Row>
				<Col xs={12} md={4} className='px-auto'>
					<Card className="cardHighlight p-2 text-center">
						<Card.Img className="w-100" variant="top" src={`https://bread-n-butter-ecommerce.herokuapp.com/${image}`} />
					</Card>
				</Col>
				<Col xs={12} md={8}>
					<Card>

					<Card.Header>
						<h4>{ name }</h4>
					</Card.Header>

						<Card.Body>
							<Card.Text>{ description }</Card.Text>
							<h6>Price: Php { price } </h6>
						</Card.Body>

						<Card.Footer className='p-0'>
							{ user.accessToken !== null ?
								<>
								<Button className="mx-1 mt-1" style={{backgroundColor: 'rgb(145, 56, 49)', borderColor:'rgb(145, 56, 49)'}} onClick={decrementQuantity}> - </Button>
										<span> {` ${quantity} `} </span>
								<Button className="mt-1 mx-1" style={{backgroundColor: 'rgb(145, 56, 49)', borderColor:'rgb(145, 56, 49)'}} onClick={incrementQuantity}> + </Button> 
									<h4>Subtotal: <span>&#8369;</span>{subTotal} </h4>
								<Button className="mx-1" style={{backgroundColor: 'rgb(145, 56, 49)', borderColor:'rgb(145, 56, 49)'}} as={Link} to="/products" > {<GrFormPreviousLink className=''/>} </Button>
								<Button className="mx-1 px-1" style={{fontWeight: 'bold', backgroundColor: 'rgb(145, 56, 49)', borderColor:'rgb(145, 56, 49)'}}  onClick={() => addToCart(productId, quantity, subTotal)}><AiOutlineShoppingCart/> Add to Cart </Button>
								<Button className="px-1 mx-1" style={{fontWeight: 'bold', backgroundColor: 'rgb(145, 56, 49)', borderColor:'rgb(145, 56, 49)'}} as={ Link } to={`/mycart`}><AiOutlineShoppingCart/> View Cart</Button>
								</>
								:
								<Button style={{backgroundColor: 'rgb(145, 56, 49)', borderColor:'rgb(145, 56, 49)'}} as={ Link } to="/login"><h4>Login to Purchase</h4></Button>
							}
								
						</Card.Footer>
					</Card>
				</Col>
			</Row>
		</Container>
		)
		
}