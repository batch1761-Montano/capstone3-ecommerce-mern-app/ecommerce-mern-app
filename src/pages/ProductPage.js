import UserView from "../components/UserView";
import AdminView from "../components/AdminView";
import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import {Row} from "react-bootstrap";



export default function ProductPage() {

	const [ allProducts, setAllProducts ] = useState([])

    const fetchData = () => {
        fetch('https://bread-n-butter-ecommerce.herokuapp.com/products/all-products')
        .then( res => res.json())
        .then(data => {
            console.log(data)
            //storing all the data to our useState allProducts
            setAllProducts(data)
        })
    }

    //it renders the function fetchData() => it gets the updated data coming from the fetch
    useEffect(() => {
        fetchData()
    }, [])
    //if the useEffect has no variables, it will only render one time
    
    const { user } = useContext(UserContext);
    
    return(
        <>
        <h1 className="text-center" style={{ fontWeight: 'bold', marginLeft: '10px'}}>Products</h1>
        <Row className="gx-4 mx-auto justify-content-center">
            
            {(user.isAdmin === true) ?

                <AdminView productsData={allProducts} fetchData={fetchData} />

                :

                <UserView productsData={allProducts} />
            }
           
        </Row>
        </>


        )
    }
