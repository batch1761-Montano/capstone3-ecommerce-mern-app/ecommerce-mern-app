import './App.css';
import { useEffect, useState } from 'react';
import CartView from './pages/CartView';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import ProductPage from './pages/ProductPage';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import SpecificProduct from './pages/SpecificProduct';
import OrdersPage from './pages/OrderPage';
import { UserProvider } from './UserContext';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App() {

   const [ user, setUser ] = useState({
        accessToken: localStorage.getItem('accessToken'),
        isAdmin: localStorage.getItem('isAdmin') === 'true'
    })

    //function for clearing localStorage on logout
    const unsetUser = () => {
        localStorage.clear();
    }

  return (
    <UserProvider value = {{ user, setUser, unsetUser }} >
      <BrowserRouter>
         <AppNavbar />  
            <>  
                <Routes>
                    <Route path="/" element={ <Home /> }/>
                    <Route path="/register" element={ <Register /> }/>
                    <Route path="/products" element={ <ProductPage /> }/>
                    <Route path="/products/:productId" element={ <SpecificProduct /> }/>
                    <Route path="/login" element={ <Login /> }/>
                    <Route path="/logout" element={ <Logout /> }/>
                    <Route path="/mycart" element={ <CartView /> }/>
                    <Route path="/myorders" element={ <OrdersPage /> }/>
                    <Route path="*" element={ <ErrorPage /> } />
                </Routes>
            </>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
