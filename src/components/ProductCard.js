import { Card, Button,} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Col, Row } from 'react-bootstrap';
import { TbDetails } from 'react-icons/tb'

export default function ProductCard({productProp}) {

ProductCard.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		image: PropTypes.string.isRequired
	})
}

//Deconstruct the product properties into their own variables
const { _id, name, description, price, image } = productProp;
		
return(
		
		<Row style={{ alignItems: 'center'}}>
			<Col>
				<Card className="mb-2 mx-auto" style={{ width: '15rem'}}>
					<Card.Img className="img-fluid rounded" variant="top" src={`https://bread-n-butter-ecommerce.herokuapp.com/${image}`} />
					<Card.Body >
						<Card.Title> { name } </Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text> { description } </Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>Php  { price } </Card.Text>
							<Button className=""  style={{fontWeight: 'bold',backgroundColor: 'rgb(145, 56, 49)', borderColor:'rgb(145, 56, 49)' }} as={ Link } to={`/products/${_id}`}><TbDetails className='mb-1'/> Details</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		
		)
}


