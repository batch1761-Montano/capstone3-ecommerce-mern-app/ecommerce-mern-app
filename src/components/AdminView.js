import { useState, useEffect } from 'react';
import { Table,Container } from 'react-bootstrap';
import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';


export default function AdminView(props) {

	const { productsData, fetchData } = props;

	const [ products, setProducts ] = useState([])


	useEffect(() => {

		const productsArr = productsData.map(product => {
			return(
				<tr key={product._id} className="text-center"  style={{borderColor: '#913831' }}>
					<td className="bg-light text-success" style={{fontWeight: 'bold'}}>{product._id}</td>
					<td>{product.name}</td>
					<td>
						<img 
						src={`https://bread-n-butter-ecommerce.herokuapp.com/${product.image}`}
						width="150px"
						/>
					</td>

					<td>{product.description}</td>
					<td>{parseFloat(product.price).toFixed(2)}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Available" : "Unavailable"}
					</td>
					<td>
						<EditProduct product={product._id} fetchData={fetchData}/>
					</td>
					<td>
						<ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData} />
					</td>
				</tr>
				)
		})
		setProducts(productsArr)
	}, [productsData])


	return(
		<>
			<div className="text-center my-3">
				<h1>Admin Dashboard</h1>
				<AddProduct fetchData={fetchData} />
				
			</div>
			
			<Table striped bordered hover responsive >
				<thead className=" text-white" style={{backgroundColor: '#913831'}}>
					<tr className="text-center">
						<th>ID</th>
						<th>NAME</th>
						<th>PRODUCT IMAGE</th>
						<th>DESCRIPTION</th>
						<th>PRICE</th>
						<th>AVAILABILITY</th>
						<th colSpan="2">ACTIONS</th>
					</tr>
				</thead>

				<tbody>
					{ products }
				</tbody>
			</Table>

		</>

		)
}