import { Row, Col, Card, Container } from 'react-bootstrap';


export default function Highlights(){
	return(
		<Container fluid className='mt-3 text-center mb-3'>
			<Row id="highlights"> 
			
				<Col xs={12} md={4} >
					<Card className="cardHighlight  mr-2">
					<Card.Img variant="top" src="./img/cover1.jpg" />
						<Card.Body>
							<Card.Title>
								<h2>Ube Pandesal</h2>
							</Card.Title>

							<Card.Text>
							Ube Pandesal with cheese are soft, fluffy, and loaded with purple yam flavor. 
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4} >
					<Card className="cardHighlight  mr-2">
					<Card.Img variant="top" src="./img/cover2.jpg" />
						<Card.Body>
							<Card.Title>
								<h2>Ube Ensaymada</h2>
							</Card.Title>

							<Card.Text>
							Ube Ensaymada is a Filipino type of brioche called ensaymada mixed with ube extract and filled with ube jam then topped with loads of grated edam cheese
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlight mr-2">
					<Card.Img variant="top" src="./img/cover3.jpg" />
						<Card.Body>
							<Card.Title>
								<h2>Korean Bun</h2>
							</Card.Title>

							<Card.Text>
							Korean Garlic Bread is made by filling round buns with softened Cream Cheese and dunking the entire bread in a butter made with Garlic, Milk, Honey, Egg, Parmesan, dry Parsley and Salt. 
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				
			</Row>
		</Container>
		)
}  
