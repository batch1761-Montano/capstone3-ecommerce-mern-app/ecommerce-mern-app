import { useState, useContext, useEffect } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { GiArchiveRegister } from "react-icons/gi";
import { AiOutlineShoppingCart } from "react-icons/ai";
import { BiLogInCircle } from "react-icons/bi";
import { AiOutlineHome } from "react-icons/ai";
import { BiLogOut } from "react-icons/bi";
import { FaRegUserCircle } from "react-icons/fa";
import { MdProductionQuantityLimits } from "react-icons/md";
import { SiCakephp } from "react-icons/si";

export default function AppNavbar() {

	const { user } = useContext(UserContext);
	const [name, setName] = useState('');

	useEffect(() => {
		fetch('https://bread-n-butter-ecommerce.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			let name = data.email.split('@')
			setName(name[0].charAt(0).toUpperCase() + name[0].slice(1))
		})
		setName(name)
	}, [name])


	return(
		<Navbar expand="lg" className="color-nav " variant="dark">
			<img alt="" src="/img/BnB_logo.jpeg"width="40"height="40"className="d-inline-block align-top ms-3"/>{' '}
			<Navbar.Brand className="ms-2"><h6>Bread&Butter </h6></Navbar.Brand>
			 
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto">
					<Nav.Link as={Link} to="/" className="mx-2"><h6><AiOutlineHome/> Home</h6></Nav.Link>
					<Nav.Link as={Link} to="/products" className="mx-2"><h6><SiCakephp/> Products</h6></Nav.Link>
					
					  {(user.accessToken === null)
					  ? <>
							<Nav.Link as={Link} to="/login"><h6><BiLogInCircle /> {`𝗟𝗼𝗴𝗶𝗻`} </h6></Nav.Link>
							<Nav.Link as={Link} to="/register"><h6><GiArchiveRegister/> {`𝗥𝗲𝗴𝗶𝘀𝘁𝗲𝗿`} </h6></Nav.Link>
						</>
					  : <>
					      {(user.isAdmin === true)
					        ? 
					        <>
					          <Nav.Link as={Link} to="/logout"><h6><BiLogOut /> {`𝗟𝗼𝗴𝗼𝘂𝘁`}</h6></Nav.Link>
					          <Nav.Link as={Link} to="/"><h6> <FaRegUserCircle /> {`Hello, ${name}`} </h6></Nav.Link>
					         </>
					        : 
					        	<>
					        <Nav.Link as={Link} to="/mycart"className="mx-2"><h6><AiOutlineShoppingCart/> {`𝗖𝗮𝗿𝘁`} </h6></Nav.Link>
					        <Nav.Link as={Link} to="/myorders"className="mx-2"><h6><MdProductionQuantityLimits/> {`𝗢𝗿𝗱𝗲𝗿𝘀`}</h6></Nav.Link>
							<Nav.Link as={Link} to="/logout" className="mx-2"><h6><BiLogOut /> Logout </h6></Nav.Link>
							<Nav.Link as={Link} to="/" className="mx-2"><h6> <FaRegUserCircle /> {`Hello, ${name}`} </h6></Nav.Link>
							</>
					      }
					    </>
					}

				</Nav>
			</Navbar.Collapse>
		</Navbar>

	)
}


