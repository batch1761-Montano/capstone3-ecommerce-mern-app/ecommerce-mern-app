import React from "react";
import {Box, Container, Row, Column, FooterLink, Heading,} from "./FooterStyles";
import { AiOutlineFacebook } from "react-icons/ai";
import { AiOutlineInstagram } from "react-icons/ai";
import { FaTiktok} from "react-icons/fa";



const Footer = () => {
  return (
    <>
    <Box>
      <h1 style={{ color: "white", textAlign: "center", marginTop: "-50px" }}> 
      Bread&Butter By Lola Ivie 
      </h1>
      <Container>
        <Row>
          <Column>
            <Heading>About Us</Heading>
              <p style={{ color: "white", textAlign: "justify" }}>We offer homemade, freshly baked breads from meticulously planned recipes. All our breads are straight from the oven provided to you still hot. Enjoy our breads as much as we enjoy serving you! </p>
          </Column>
          <Column>
            <Heading>Products</Heading>
            <FooterLink href="#">Breads</FooterLink>
            <FooterLink href="#">Cakes</FooterLink>
            <FooterLink href="#">Mini-Cakes</FooterLink>

          </Column>
          <Column>
            <Heading>Contact</Heading>
            <FooterLink href="#">@BreadnButter</FooterLink>
            <FooterLink href="#">ivie26@yahoo.com</FooterLink>
            <FooterLink href="#">0915-719-1664</FooterLink>
          </Column>
          <Column>
            <Heading>Social Media</Heading>
            <FooterLink href="#">
              <i className="fab fa-facebook-f">
                <span style={{ marginLeft: "10px" }}>
                <AiOutlineFacebook /> Facebook
                </span>
              </i>
            </FooterLink>
            <FooterLink href="#">
              <i className="fab fa-instagram">
                <span style={{ marginLeft: "10px" }}>
                <AiOutlineInstagram /> Instagram
                </span>
              </i>
            </FooterLink>
            <FooterLink href="#">
              <i className="fab fa-twitter">
                <span style={{ marginLeft: "10px" }}>
                <FaTiktok /> Tiktok
                </span>
              </i>
            </FooterLink>
        

          </Column>
        </Row>    
        <h6 style={{ color: "white",
                     textAlign: "center", 
                     marginTop: "0px",
                     fontWeight: "bold" }}>
       <p>Copyright &copy; 2022 Bread&Butter. All Rights Reserved</p>
        </h6>
      </Container>
    </Box>
    </>
  )
}

export default Footer;
