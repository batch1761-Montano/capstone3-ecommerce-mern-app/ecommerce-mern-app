import React from 'react';
import { Card, Button, Row, Col, Image, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner() {
    return (
        
        <div
        id='intro-example'
        className='p-5 text-center bg-image'
        style={{ backgroundImage: "url('./img/Kbun3.jpg')", backgroundPosition: 'center', backgroundSize: '100%', backgroundRepeat: 'no-repeat' ,width: 'auto', height:'auto'}}
      >
        <div className='mask' style={{ backgroundColor: 'rgba(0, 0, 0, 0.7)' }}>
          <div className='d-flex justify-content-center align-items-center h-100'>
            <div className='text-white'>
              <h1 className='mb-3'>Welcome to Bread & Butter</h1>
             
              <a
                className='btn btn-outline-light btn-lg m-2'
                style={{fontWeight: 'bold'}}
                href='https://bread-n-butter.vercel.app/products'
                role='button'
                rel='nofollow'
                target='_blank'
              >
                View for more
              </a>
            </div>
          </div>
        </div>
      </div>
  
        
    )
}