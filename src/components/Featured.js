import React from 'react';
import { useState, useContext } from 'react';
//React Bootstrap components
import { Navbar, Nav } from 'react-bootstrap';
//react-router
import { Link } from 'react-router-dom';
import { Carousel } from 'react-bootstrap';
import { Col, Container } from 'react-bootstrap';

export default function AppNavbar() {

	return(
		<Container fluid className='w-50'>
			<Col xs={12} md={6} lg={2} className="carousel p-2 w-auto m-2">
			<Carousel fade variant="light" className="h-25 w-auto">
				<Carousel.Item>
					<img
					className="d-block w-100 h-25"
					src="/img/cover1.jpg"
					alt="First slide"
					/>
					<Carousel.Caption>
					<h3>Ube Pandesal</h3>
					<p></p>
					</Carousel.Caption>
				</Carousel.Item>

				<Carousel.Item>
					<img
					className="d-block w-100 h-25"
					src="/img/cover2.jpg"
					alt="Second slide"
					/>

					<Carousel.Caption>
					<h3>Ube Ensaymada</h3>
					<p></p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
					className="d-block w-100 h-25"
					src="/img/cover3.jpg"
					alt="Third slide"
					/>

					<Carousel.Caption>
					<h3>Korean Bun</h3>
					<p></p>
					</Carousel.Caption>
				</Carousel.Item>
			</Carousel>
			</Col>
		</Container>
	)
}


